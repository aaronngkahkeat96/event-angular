import { HttpClient } from "@angular/common/http";
import { EventEmitter, Injectable, Output } from "@angular/core";
import { Observable, Subject } from 'rxjs';

@Injectable()
export class EventService {
    constructor(private http: HttpClient
    ) {
    }


    getEvents(): Observable<any> {
        return this.http.get("https://app.ticketmaster.com/discovery/v2/events.json?size=30&apikey=AJIubp2Y9E8NY4rBrYmVt2nJqHjghF8S");
      }

      getEventDetails(id:string): Observable<any> {
        return this.http.get(`https://app.ticketmaster.com/discovery/v2/events/${id}.json?apikey=AJIubp2Y9E8NY4rBrYmVt2nJqHjghF8S`);
      }

}
