import { Component, Inject, AfterViewInit, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import {EventService} from '../../service/event.service'

@Component({
  selector: "event-details-dialog",
  templateUrl: "event-details-dialog.html",
  providers:[EventService]
})
export class EventDetailsDialog implements AfterViewInit{
  event: any = {};
  imgUrl : string = 'https://cloudfront-us-east-1.images.arcpublishing.com/coindesk/WMXJCFJ3ERCETA6TJNZ5NQPNKA.webp';

  constructor(
    public dialogRef: MatDialogRef<EventDetailsDialog>,
    private EventService : EventService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
  }

  private getData(){
    this.EventService.getEventDetails(this.data.id).subscribe(data => {
      this.event = data;
      this.imgUrl = data?.images[0]?.url;
    });
  };

  ngAfterViewInit(): void {
    this.getData();
  };
}
