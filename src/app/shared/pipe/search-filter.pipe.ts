import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'SearchFilter',
  pure: false
})
export class SearchFilterPipe implements PipeTransform {
  transform(items: any[], filter: any): any {
    if (!items || !filter) {
      return items;
    }
    return items.filter((item) => {
      return JSON.stringify(item).toLowerCase().
        includes(filter.toLowerCase());
    }
    );
  }
} 
